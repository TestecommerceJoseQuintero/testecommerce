import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import accounting from "accounting";
import pluralize from "pluralize";

import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

Vue.config.productionTip = false;

Vue.filter("formatMoney", accounting.formatMoney);
Vue.filter("pluralize", pluralize);

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");
