<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductsRequest;
use App\Http\Resources\ProductsCollection;
use App\Http\Resources\ProductsResource;
use App\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class ProductsController extends Controller
{
    public function index()
    {       
        return ProductsCollection::collection(Products::paginate(5));
    }
    public function store(ProductsRequest $request)
    {
       $products = new Products;
       $products->name = $request->name;
       $products->detail = $request->description;
       $products->price = $request->price;
       $products->stock = $request->stock;
       $products->discount = $request->discount;

       $products->save();

       return response([

         'data' => new ProductsResource($products)

       ],Response::HTTP_CREATED);

    }

    public function show(Products $products)
    {
        return new ProductsResource($products);
    }

    public function update(Request $request, Products $products)
    {   
        $request['detail'] = $request->description;

        unset($request['description']);

        $products->update($request->all());

       return response([

         'data' => new ProductsResource($products)

       ],Response::HTTP_CREATED);

    }

    public function destroy(Products $products)
    {
        $products->delete();

        return response(null,Response::HTTP_NO_CONTENT);
    }
}
