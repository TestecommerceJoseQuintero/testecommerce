<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'price' => $this->price,
            'inventory' => $this->inventory,
            'shipping' => $this->shipping,
            'description' => $this->description,
            'status' => $this->status,
            /* 'href' => [
               'link' => route('products.show',$this->id)
            ] */
        ];        
    }
}
