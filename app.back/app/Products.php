<?php

namespace App;
use App\Products;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = [
        'id', 'name', 'slug', 'shipping' , 'inventory' , 'price','description','status'
    ];
    public function products()
    {
    	return $this->belongsTo(Products::class);
    }
}