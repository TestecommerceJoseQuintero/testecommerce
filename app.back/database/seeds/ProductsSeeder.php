<?php

use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //guardar un solo registro
        DB::table('products')->insert([
            'name' => Str::random(10),
            'slug' => Str::random(10),
            'description' => Str::random(50),
            'price' => $this->rand_float(20,50),
            'status' => 1,
        ]);

        //guardar 20 registros
        //$arrays = range(0,20);
        $arrays = array(
                    array("id"=> 1, "title"=> "iPad 4 Mini", "price"=> 500.01, "inventory"=> 2, "shipping"=> 15.00 ),                    
                    array( "id"=> 2, "title"=> "H&M T-Shirt White", "price"=> 10.99, "inventory"=> 10, "shipping"=> 5.00 ),
                    array( "id"=> 3, "title"=> "Nirvana - LP", "price"=> 19.99, "inventory"=> 3, "shipping"=> 22.50 ),
                    array( "id"=> 4, "title"=> "Licensed Steel Gloves", "price"=> 30.99, "inventory"=> 5, "shipping"=> 9.00 ),
                    array( "id"=> 5, "title"=> "Rustic Granite Car", "price"=> 487.00, "inventory"=> 1, "shipping"=> 35.00 ),
                    array( "id"=> 6, "title"=> "Fantastic Cotton Pants", "price"=> 59.59, "inventory"=> 6, "shipping"=> 11.00 ),
                    array( "id"=> 7, "title"=> "Tasty Wooden Pizza", "price"=> 29.00, "inventory"=> 2, "shipping"=> 18.00 ),
                    array( "id"=> 8, "title"=> "Delicious Concrete Fish", "price"=> 12.99, "inventory"=> 4, "shipping"=> 6.00 ),
                    array( "id"=> 9, "title"=> "Granite Computer", "price"=> 109.10, "inventory"=> 10, "shipping"=> 22.70 ),
                    array( "id"=> 10, "title"=> "Handcrafted Soft Salad", "price"=> 13.99, "inventory"=> 3, "shipping"=> 3.50 ),
                    array( "id"=> 11, "title"=> "Incredible Steel Bacon", "price"=> 30.99, "inventory"=> 5, "shipping"=> 7.90 ),
                    array( "id"=> 12, "title"=> "Tasty Plastic Bike", "price"=> 75.00, "inventory"=> 5, "shipping"=> 25.00 )
        );
        foreach ($arrays as $valor) {
            //$randData = $this->rand_product();            
            /* DB::table('products')->insert([	            
                'name' => Str::random(10),//$randData[0],
                'description' => Str::random(50),//$randData[1],
                'slug' => Str::random(10),//$randData[2],                
                'price' => $this->rand_float(20,50),
                'status' => 1,
            ]); */
            //var_dump($valor);
            DB::table('products')->insert([	            
                'name' => $valor["title"],//$randData[0],
                'description' => (string)$valor["title"]."-".(string)$valor["price"]."-".(string)$valor["inventory"]."-".(string)$valor["shipping"],//$randData[1],
                'slug' => Str::random(10),//$randData[2],   
                'shipping' => $valor["shipping"],//$randData[2],
                'price' => $valor["price"],
                'inventory' => $valor["inventory"],
                'status' => 1,
            ]);
        }
    }

    function rand_float($st_num=0,$end_num=1,$mul=1000000)
    {
        if ($st_num>$end_num) return false;
        return mt_rand($st_num*$mul,$end_num*$mul)/$mul;
    }
}
